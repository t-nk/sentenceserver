package sentence.dipMjl.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
public class Accreditation implements Serializable{
	@Id
	private String code;
	private boolean lectureCondamnation; 
	private boolean ecritureCondamnation;
	private boolean lectureDonnees;
	private boolean ecritureDonnees;
	private boolean modificationUtilisateurs;
	private boolean lectureStatistiques;
	
	//@OneToOne
	private Utilisateur utilisateur;
	
	//@ManyToOne
	private Groupe groupe;

	
	public Accreditation() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Accreditation(String code, boolean lectureCondamnation, boolean ecritureCondamnation, boolean lectureDonnees,
			boolean ecritureDonnees, boolean modificationUtilisateurs, boolean lectureStatistiques,
			Utilisateur utilisateur, Groupe groupe) {
		super();
		this.code = code;
		this.lectureCondamnation = lectureCondamnation;
		this.ecritureCondamnation = ecritureCondamnation;
		this.lectureDonnees = lectureDonnees;
		this.ecritureDonnees = ecritureDonnees;
		this.modificationUtilisateurs = modificationUtilisateurs;
		this.lectureStatistiques = lectureStatistiques;
		this.utilisateur = utilisateur;
		this.groupe = groupe;
	}


	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public boolean isLectureCondamnation() {
		return lectureCondamnation;
	}


	public void setLectureCondamnation(boolean lectureCondamnation) {
		this.lectureCondamnation = lectureCondamnation;
	}


	public boolean isEcritureCondamnation() {
		return ecritureCondamnation;
	}


	public void setEcritureCondamnation(boolean ecritureCondamnation) {
		this.ecritureCondamnation = ecritureCondamnation;
	}


	public boolean isLectureDonnees() {
		return lectureDonnees;
	}


	public void setLectureDonnees(boolean lectureDonnees) {
		this.lectureDonnees = lectureDonnees;
	}


	public boolean isEcritureDonnees() {
		return ecritureDonnees;
	}


	public void setEcritureDonnees(boolean ecritureDonnees) {
		this.ecritureDonnees = ecritureDonnees;
	}


	public boolean isModificationUtilisateurs() {
		return modificationUtilisateurs;
	}


	public void setModificationUtilisateurs(boolean modificationUtilisateurs) {
		this.modificationUtilisateurs = modificationUtilisateurs;
	}


	public boolean isLectureStatistiques() {
		return lectureStatistiques;
	}


	public void setLectureStatistiques(boolean lectureStatistiques) {
		this.lectureStatistiques = lectureStatistiques;
	}


	public Utilisateur getUtilisateur() {
		return utilisateur;
	}


	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}


	public Groupe getGroupe() {
		return groupe;
	}


	public void setGroupe(Groupe groupe) {
		this.groupe = groupe;
	}
	
	
}

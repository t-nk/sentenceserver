package sentence.dipMjl.entities;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Execution implements Serializable{
	@Id
	private String code;
	private Date dateExecution;
	private String statutExecution;
	private Date dateFinExecution;
	private String numeroEcrou; 
	
	private Peine peine;
	private MaisonArret maisonArret;
	
	public Execution() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getDateExecution() {
		return dateExecution;
	}

	public void setDateExecution(Date dateExecution) {
		this.dateExecution = dateExecution;
	}

	public String getStatutExecution() {
		return statutExecution;
	}

	public void setStatutExecution(String statutExecution) {
		this.statutExecution = statutExecution;
	}

	public Date getDateFinExecution() {
		return dateFinExecution;
	}

	public void setDateFinExecution(Date dateFinExecution) {
		this.dateFinExecution = dateFinExecution;
	}

	public String getNumeroEcrou() {
		return numeroEcrou;
	}

	public void setNumeroEcrou(String numeroEcrou) {
		this.numeroEcrou = numeroEcrou;
	}

	public Peine getPeine() {
		return peine;
	}

	public void setPeine(Peine peine) {
		this.peine = peine;
	}

	public MaisonArret getMaisonArret() {
		return maisonArret;
	}

	public void setMaisonArret(MaisonArret maisonArret) {
		this.maisonArret = maisonArret;
	}
	
	
	
}

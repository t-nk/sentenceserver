package sentence.dipMjl.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Pays implements Serializable{
	
	@Id
	private String code;
	private String nom;
	private int indicatif;

	public Pays() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Pays(String code, String nom, int indicatif) {
		super();
		this.code = code;
		this.nom = nom;
		this.indicatif = indicatif;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getIndicatif() {
		return indicatif;
	}

	public void setIndicatif(int indicatif) {
		this.indicatif = indicatif;
	}
}

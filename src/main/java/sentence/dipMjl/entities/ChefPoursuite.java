package sentence.dipMjl.entities;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class ChefPoursuite implements Serializable {
	@Id
	private String code;
	private Date dateInfraction;
	private String details;
	
	private Qualite qualite;
	private Personne procureur;
	private Adresse lieuInfraction;
	private Infraction infraction;
	
	public ChefPoursuite() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ChefPoursuite(String code, Date dateInfraction, String details, Qualite qualite, Personne procureur,
			Adresse lieuInfraction, Infraction infraction) {
		super();
		this.code = code;
		this.dateInfraction = dateInfraction;
		this.details = details;
		this.qualite = qualite;
		this.procureur = procureur;
		this.lieuInfraction = lieuInfraction;
		this.infraction = infraction;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getDateInfraction() {
		return dateInfraction;
	}

	public void setDateInfraction(Date dateInfraction) {
		this.dateInfraction = dateInfraction;
	}
	

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public Qualite getQualite() {
		return qualite;
	}

	public void setQualite(Qualite qualite) {
		this.qualite = qualite;
	}

	public Personne getProcureur() {
		return procureur;
	}

	public void setProcureur(Personne procureur) {
		this.procureur = procureur;
	}

	public Adresse getLieuInfraction() {
		return lieuInfraction;
	}

	public void setLieuInfraction(Adresse lieuInfraction) {
		this.lieuInfraction = lieuInfraction;
	}

	public Infraction getInfraction() {
		return infraction;
	}

	public void setInfraction(Infraction infraction) {
		this.infraction = infraction;
	}
}

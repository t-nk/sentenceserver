package sentence.dipMjl.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Peine implements Serializable {
	
	@Id
	private String code;
	private int montant;
	private int duree;
	
	private TypePeine typePeine;
	//@OneToOne
	private Condamnation condamnation; 
	
	public Peine() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Peine(String code, int montant, int duree, TypePeine typePeine, Condamnation condamnation) {
		super();
		this.code = code;
		this.montant = montant;
		this.duree = duree;
		this.typePeine = typePeine;
		this.condamnation = condamnation;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getMontant() {
		return montant;
	}

	public void setMontant(int montant) {
		this.montant = montant;
	}

	public int getDuree() {
		return duree;
	}

	public void setDuree(int duree) {
		this.duree = duree;
	}

	public TypePeine getTypePeine() {
		return typePeine;
	}

	public void setTypePeine(TypePeine typePeine) {
		this.typePeine = typePeine;
	}

	public Condamnation getCondamnation() {
		return condamnation;
	}

	public void setCondamnation(Condamnation condamnation) {
		this.condamnation = condamnation;
	}	
}


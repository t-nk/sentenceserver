package sentence.dipMjl.entities;


import java.io.Serializable;
import java.sql																																																																																																																																																																																																				.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

@Entity
public class Affaire implements Serializable {
	
	@Id
	private String code;
	private String numero;
	private Date dateDebutAffaire;
	
	private Juridiction juridiction;
	//@OneToMany(mappedBy="affaire")
	//private List<ChefPoursuite> chefPoursuites;
	private ChefPoursuite chefPoursuite;
	
	public Affaire() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Affaire(String code, String numero, Date dateDebutAffaire, Juridiction juridiction,
			ChefPoursuite chefPoursuite) {
		super();
		this.code = code;
		this.numero = numero;
		this.dateDebutAffaire = dateDebutAffaire;
		this.juridiction = juridiction;
		this.chefPoursuite = chefPoursuite;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Date getDateDebutAffaire() {
		return dateDebutAffaire;
	}

	public void setDateDebutAffaire(Date dateDebutAffaire) {
		this.dateDebutAffaire = dateDebutAffaire;
	}

	public Juridiction getJuridiction() {
		return juridiction;
	}

	public void setJuridiction(Juridiction juridiction) {
		this.juridiction = juridiction;
	}

	public ChefPoursuite getChefPoursuite() {
		return chefPoursuite;
	}

	public void setChefPoursuite(ChefPoursuite chefPoursuite) {
		this.chefPoursuite = chefPoursuite;
	}
}

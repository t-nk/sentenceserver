package sentence.dipMjl.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class UniteAdministrative implements Serializable {

	@Id
	private String code;
	private String denomination;
	private int nombreHabitants;
	private UniteAdministrative uaMere;
	
	private Pays pays;

	public UniteAdministrative() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UniteAdministrative(String code, String denomination, int nombreHabitants, UniteAdministrative uaMere,
			Pays pays) {
		super();
		this.code = code;
		this.denomination = denomination;
		this.nombreHabitants = nombreHabitants;
		this.uaMere = uaMere;
		this.pays = pays;
	}

	public UniteAdministrative(String code, String denomination) {
		super();
		this.code = code;
		this.denomination = denomination;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDenomination() {
		return denomination;
	}

	public void setDenomination(String denomination) {
		this.denomination = denomination;
	}

	public int getNombreHabitants() {
		return nombreHabitants;
	}

	public void setNombreHabitants(int nombreHabitants) {
		this.nombreHabitants = nombreHabitants;
	}

	public UniteAdministrative getUaMere() {
		return uaMere;
	}

	public void setUaMere(UniteAdministrative uaMere) {
		this.uaMere = uaMere;
	}

	public Pays getPays() {
		return pays;
	}

	public void setPays(Pays pays) {
		this.pays = pays;
	}
	
}

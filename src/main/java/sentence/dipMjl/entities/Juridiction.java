package sentence.dipMjl.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Juridiction implements Serializable{
	
	@Id
	private String code;
	private String denomination;
	private String type;
	
	private Juridiction juridictionPere;

	public Juridiction() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Juridiction(String code, String denomination, String type, Juridiction juridictionPere) {
		super();
		this.code = code;
		this.denomination = denomination;
		this.type = type;
		this.juridictionPere = juridictionPere;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDenomination() {
		return denomination;
	}

	public void setDenomination(String denomination) {
		this.denomination = denomination;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Juridiction getJuridictionPere() {
		return juridictionPere;
	}

	public void setJuridictionPere(Juridiction juridictionPere) {
		this.juridictionPere = juridictionPere;
	}
}

package sentence.dipMjl.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Qualite implements Serializable {

	@Id
	private String code;
	private String libele;
	private String groupe;
	
	public String getCode() {
		return code;
	}
	
	public Qualite(String code, String libele, String groupe) {
		super();
		this.code = code;
		this.libele = libele;
		this.groupe = groupe;
	}

	public Qualite() {
		super();
		// TODO Auto-generated constructor stub
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLibele() {
		return libele;
	}

	public void setLibele(String libele) {
		this.libele = libele;
	}

	public String getGroupe() {
		return groupe;
	}

	public void setGroupe(String groupe) {
		this.groupe = groupe;
	}

	
	
}

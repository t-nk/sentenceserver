package sentence.dipMjl.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

@Entity
public class Infraction implements Serializable{
	
	@Id
	private String code;
	private String libele;
	private String categorie;
	private String famille;
	
	@ManyToOne
	private ChefPoursuite chefPoursuite;
	
	public String getCode() {
		return code;
	}
	
	public Infraction(String code, String libele, String categorie, String famille) {
		super();
		this.code = code;
		this.libele = libele;
		this.categorie = categorie;
		this.famille = famille;
	}

	public Infraction() {
		super();
		// TODO Auto-generated constructor stub
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLibele() {
		return libele;
	}

	public void setLibele(String libele) {
		this.libele = libele;
	}

	public String getCategorie() {
		return categorie;
	}

	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}

	public String getFamille() {
		return famille;
	}

	public void setFamille(String famille) {
		this.famille = famille;
	}
}

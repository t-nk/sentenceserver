package sentence.dipMjl.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Groupe implements Serializable{
	
	@Id
	private String code;
	private String libele;
	private boolean lectureCondamnation;
	private boolean ecritureCondamnation;
	private boolean lectureDonnees;
	private boolean ecritureDonnees;
	private boolean manipulationUtilisateurs;
	private boolean lectureStatistiques;
	private boolean manipulationStatistiques;
	
	public Groupe() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Groupe(String code, String libele, boolean lectureCondamnation, boolean ecritureCondamnation,
			boolean lectureDonnees, boolean ecritureDonnees, boolean manipulationUtilisateurs,
			boolean lectureStatistiques, boolean manipulationStatistiques) {
		super();
		this.code = code;
		this.libele = libele;
		this.lectureCondamnation = lectureCondamnation;
		this.ecritureCondamnation = ecritureCondamnation;
		this.lectureDonnees = lectureDonnees;
		this.ecritureDonnees = ecritureDonnees;
		this.manipulationUtilisateurs = manipulationUtilisateurs;
		this.lectureStatistiques = lectureStatistiques;
		this.manipulationStatistiques = manipulationStatistiques;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLibele() {
		return libele;
	}

	public void setLibele(String libele) {
		this.libele = libele;
	}

	public boolean isLectureCondamnation() {
		return lectureCondamnation;
	}

	public void setLectureCondamnation(boolean lectureCondamnation) {
		this.lectureCondamnation = lectureCondamnation;
	}

	public boolean isEcritureCondamnation() {
		return ecritureCondamnation;
	}

	public void setEcritureCondamnation(boolean ecritureCondamnation) {
		this.ecritureCondamnation = ecritureCondamnation;
	}

	public boolean isLectureDonnees() {
		return lectureDonnees;
	}

	public void setLectureDonnees(boolean lectureDonnees) {
		this.lectureDonnees = lectureDonnees;
	}

	public boolean isEcritureDonnees() {
		return ecritureDonnees;
	}

	public void setEcritureDonnees(boolean ecritureDonnees) {
		this.ecritureDonnees = ecritureDonnees;
	}

	public boolean isManipulationUtilisateurs() {
		return manipulationUtilisateurs;
	}

	public void setManipulationUtilisateurs(boolean manipulationUtilisateurs) {
		this.manipulationUtilisateurs = manipulationUtilisateurs;
	}

	public boolean isLectureStatistiques() {
		return lectureStatistiques;
	}

	public void setLectureStatistiques(boolean lectureStatistiques) {
		this.lectureStatistiques = lectureStatistiques;
	}

	public boolean isManipulationStatistiques() {
		return manipulationStatistiques;
	}

	public void setManipulationStatistiques(boolean manipulationStatistiques) {
		this.manipulationStatistiques = manipulationStatistiques;
	}
}

package sentence.dipMjl.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class MaisonArret implements Serializable{
	@Id
	private String code;
	private String libele;
	
	public MaisonArret() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MaisonArret(String code, String libele) {
		super();
		this.code = code;
		this.libele = libele;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLibele() {
		return libele;
	}

	public void setLibele(String libele) {
		this.libele = libele;
	}
}

package sentence.dipMjl.entities;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Condamnation implements Serializable{
	
	@Id
	private String code;
	private Date dateSignificationDecision;
	private String natureSignificationDecision;

	private Affaire affaire;
	private Personne condamne;
	private Personne beneficiaire;
	
	
	public Condamnation() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Condamnation(String code, Date dateSignificationDecision, String natureSignificationDecision,
			Affaire affaire, Personne condamne, Personne beneficiaire) {
		super();
		this.code = code;
		this.dateSignificationDecision = dateSignificationDecision;
		this.natureSignificationDecision = natureSignificationDecision;
		this.affaire = affaire;
		this.condamne = condamne;
		this.beneficiaire = beneficiaire;
	}


	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public Date getDateSignificationDecision() {
		return dateSignificationDecision;
	}


	public void setDateSignificationDecision(Date dateSignificationDecision) {
		this.dateSignificationDecision = dateSignificationDecision;
	}


	public String getNatureSignificationDecision() {
		return natureSignificationDecision;
	}


	public void setNatureSignificationDecision(String natureSignificationDecision) {
		this.natureSignificationDecision = natureSignificationDecision;
	}


	public Affaire getAffaire() {
		return affaire;
	}


	public void setAffaire(Affaire affaire) {
		this.affaire = affaire;
	}


	public Personne getCondamne() {
		return condamne;
	}


	public void setCondamne(Personne condamne) {
		this.condamne = condamne;
	}


	public Personne getBeneficiaire() {
		return beneficiaire;
	}


	public void setBeneficiaire(Personne beneficiaire) {
		this.beneficiaire = beneficiaire;
	}
}

package sentence.dipMjl.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Decision implements Serializable {

	@Id
	private String code;
	private String numero;	
	private String libele;
	private Date dateAudience;
	private String typeDecision;
	
	private Affaire affaire;
	private Personne juge;

	public Decision() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getLibele() {
		return libele;
	}

	public void setLibele(String libele) {
		this.libele = libele;
	}

	public Date getDateAudience() {
		return dateAudience;
	}

	public void setDateAudience(Date dateAudience) {
		this.dateAudience = dateAudience;
	}

	public Affaire getAffaire() {
		return affaire;
	}

	public void setAffaire(Affaire affaire) {
		this.affaire = affaire;
	}

	public String getTypeDecision() {
		return typeDecision;
	}

	public void setTypeDecision(String typeDecision) {
		this.typeDecision = typeDecision;
	}

	public Personne getJuge() {
		return juge;
	}

	public void setJuge(Personne juge) {
		this.juge = juge;
	}	
	
}

package sentence.dipMjl.entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Adresse implements Serializable {
	@Id
	private String code;
	private String maison;
	private int telephone;
	private String email;
	private String codePostal;
	private String rue;
	
	private UniteAdministrative uniteAdministrative;
	
	public Adresse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Adresse(String code, String maison, int telephone, String email, String codePostal, String rue,
			UniteAdministrative uniteAdministrative) {
		super();
		this.code = code;
		this.maison = maison;
		this.telephone = telephone;
		this.email = email;
		this.codePostal = codePostal;
		this.rue = rue;
		this.uniteAdministrative = uniteAdministrative;
	}


	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMaison() {
		return maison;
	}

	public void setMaison(String maison) {
		this.maison = maison;
	}

	public int getTelephone() {
		return telephone;
	}

	public void setTelephone(int telephone) {
		this.telephone = telephone;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public String getRue() {
		return rue;
	}

	public void setRue(String rue) {
		this.rue = rue;
	}

	public UniteAdministrative getUniteAdministrative() {
		return uniteAdministrative;
	}

	public void setUniteAdministrative(UniteAdministrative uniteAdministrative) {
		this.uniteAdministrative = uniteAdministrative;
	}
}

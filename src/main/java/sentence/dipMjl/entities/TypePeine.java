package sentence.dipMjl.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class TypePeine implements Serializable{
	
	@Id
	private String code;
	private String libele;

	public String getCode() {
		return code;
	}

	public TypePeine(String code, String libele) {
		super();
		this.code = code;
		this.libele = libele;
	}

	public TypePeine() {
		super();
		// TODO Auto-generated constructor stub
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLibele() {
		return libele;
	}

	public void setLibele(String libele) {
		this.libele = libele;
	}	
}

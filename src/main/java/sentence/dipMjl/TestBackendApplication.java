package sentence.dipMjl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import sentence.dipMjl.entities.*;
import sentence.dipMjl.repositories.*;

@SpringBootApplication
public class TestBackendApplication implements CommandLineRunner{
	@Autowired
	private RepositoryRestConfiguration restConfiguration;
	
	@Autowired
	private GroupeRepository groupeRepository;
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(TestBackendApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		restConfiguration.exposeIdsFor(Groupe.class);
		restConfiguration.exposeIdsFor(Infraction.class);
		restConfiguration.exposeIdsFor(Juridiction.class);
		restConfiguration.exposeIdsFor(Pays.class);
		restConfiguration.exposeIdsFor(Qualite.class);
		restConfiguration.exposeIdsFor(TypePeine.class);
		restConfiguration.exposeIdsFor(UniteAdministrative.class);
		restConfiguration.exposeIdsFor(Utilisateur.class);
		restConfiguration.exposeIdsFor(Accreditation.class);
		restConfiguration.exposeIdsFor(Adresse.class);
		restConfiguration.exposeIdsFor(Affaire.class);
		restConfiguration.exposeIdsFor(ChefPoursuite.class);
		restConfiguration.exposeIdsFor(Condamnation.class);
		restConfiguration.exposeIdsFor(Decision.class);
		restConfiguration.exposeIdsFor(Peine.class);
		restConfiguration.exposeIdsFor(Personne.class);
		restConfiguration.exposeIdsFor(MaisonArret.class);
		
		/*Stream.of("titre1", "titre2", "titre3", "titre4", "titre5").forEach(s -> {
			civiliteRepository.save(new Civilite("code",s));
		});
		juridictionRepository.findAll().forEach(juridiction -> {
			System.out.println(juridiction.toString());
		});*/
		
		//création admi par défaut
		groupeRepository.save(new Groupe("code1","administrateur",true,true,true,true,true,true,true));
		utilisateurRepository.save(new Utilisateur("code1","marcD","marcD","DA SILVA","Marc","marco@hmail.com",(long) 97456754,
				new Groupe("code1","administrateur",true,true,true,true,true,true,true)
		));
		
		System.out.println("ok");
	}
}

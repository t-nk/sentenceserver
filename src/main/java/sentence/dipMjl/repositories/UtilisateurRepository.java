package sentence.dipMjl.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import sentence.dipMjl.entities.Utilisateur;

@RepositoryRestResource
@CrossOrigin
public interface UtilisateurRepository extends JpaRepository<Utilisateur, String>{
	
	@RestResource(path = "/byNom")
	public Page<Utilisateur> findByNomContains(@Param("key") String key, Pageable pageable);
	
	@RestResource(path = "/byPrenoms")
	public Page<Utilisateur> findByPrenomsContains(@Param("key") String key, Pageable pageable);
	
	//@RestResource(path = "/byNomPrenoms")
	//public Page<Utilisateur> findByNomOrPrenomsContains(@Param("key") String key1,@Param("key") String key2, Pageable pageable);
	
	@RestResource(path = "/byMail")
	public Page<Utilisateur> findByMailContains(@Param("key") String key, Pageable pageable);
	
	@RestResource(path = "/byLogin")
	public Page<Utilisateur> findByLoginContains(@Param("key") String key, Pageable pageable);
	
	@RestResource(path = "/byPasse")
	public Page<Utilisateur> findByPasseContains(@Param("key") String key, Pageable pageable);
}

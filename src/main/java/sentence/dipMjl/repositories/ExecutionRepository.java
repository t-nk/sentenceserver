package sentence.dipMjl.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import sentence.dipMjl.entities.Execution;

@RepositoryRestResource
@CrossOrigin
public interface ExecutionRepository extends JpaRepository<Execution,String>{
	@RestResource(path = "/byNumeroEcrou")
	public Page<Execution> findByNumeroEcrouContains(@Param("key") String key, Pageable pageable);
}

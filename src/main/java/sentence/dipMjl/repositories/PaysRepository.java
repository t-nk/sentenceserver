package sentence.dipMjl.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import sentence.dipMjl.entities.Pays;

@RepositoryRestResource
@CrossOrigin
public interface PaysRepository extends JpaRepository<Pays, String> {

	@RestResource(path = "/byNom")
	public Page<Pays> findByNomContains(@Param("key") String key, Pageable pageable);
	
}

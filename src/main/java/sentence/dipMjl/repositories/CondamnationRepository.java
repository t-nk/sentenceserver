package sentence.dipMjl.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import sentence.dipMjl.entities.Condamnation;

@RepositoryRestResource
@CrossOrigin
public interface CondamnationRepository extends JpaRepository<Condamnation,String>{

}

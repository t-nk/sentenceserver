package sentence.dipMjl.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import sentence.dipMjl.entities.Juridiction;

@RepositoryRestResource
@CrossOrigin
public interface JuridictionRepository extends JpaRepository<Juridiction, String>{

	@RestResource(path = "/byDenomination")
	public Page<Juridiction> findByDenominationContains(@Param("key") String key, Pageable pageable);
}

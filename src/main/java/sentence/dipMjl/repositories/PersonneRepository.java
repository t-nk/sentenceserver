package sentence.dipMjl.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import sentence.dipMjl.entities.Personne;

@RepositoryRestResource
@CrossOrigin
public interface PersonneRepository extends JpaRepository<Personne,String> {
	@RestResource(path = "/byNom")
	public Page<Personne> findByNomContains(@Param("key") String key, Pageable pageable);
	
	@RestResource(path = "/byNomPrenoms")
	public Page<Personne> findByNomOrPrenomsContains(@Param("key") String key, Pageable pageable);
	
}

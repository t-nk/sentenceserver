package sentence.dipMjl.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import sentence.dipMjl.entities.Groupe;

@RepositoryRestResource
//@Repository
@CrossOrigin
public interface GroupeRepository extends JpaRepository<Groupe, String> {
	@RestResource(path = "/byLibeleListe")
	public List<Groupe> findByLibeleContains(@Param("key") String key);
	
	@RestResource(path = "/byLibele")
	public Page<Groupe> findByLibeleContains(@Param("key") String key, Pageable pageable);
}

package sentence.dipMjl.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import sentence.dipMjl.entities.Adresse;

@RepositoryRestResource
@CrossOrigin
public interface AdresseRepository extends JpaRepository<Adresse,String>{
	@RestResource(path = "/byMaison")
	public Page<Adresse> findByMaisonContains(@Param("key") String key, Pageable pageable);
	
	@RestResource(path = "/byCode")
	public Page<Adresse> findByCodeContains(@Param("key") String key, Pageable pageable);
}

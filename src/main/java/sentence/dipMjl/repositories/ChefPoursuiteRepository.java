package sentence.dipMjl.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import sentence.dipMjl.entities.ChefPoursuite;

@RepositoryRestResource
@CrossOrigin
public interface ChefPoursuiteRepository extends JpaRepository<ChefPoursuite,String>{
	
}
